document.addEventListener("DOMContentLoaded", main);

async function main() {
    let ascii_header_element = document.getElementById("header");
    get_ascii_logo().then(ascii_logo => 
        ascii_header_element.innerHTML = ascii_logo);
}
