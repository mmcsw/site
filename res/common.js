function get_ascii_logo() {
    return fetch("/res/small-header.txt")
        .then(response => response.text())
        .then(text => text.replace(/\u0020/g, "\u{00A0}"))
        .catch(rejected => {
            console.log(rejected);
            return "ΞIX";
        });
}